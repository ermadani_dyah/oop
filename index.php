<?php
    include('animal.php');
    include('Ape.php');
    include('Frog.php');
    $sheep = new Animal("shaun");

    echo "Nama Hewan: $sheep->name <br>"; // "shaun"
    echo "Jumlah Kaki: $sheep->legs <br>"; // 4
    echo "Berdarah Dingin: $sheep->cold_blooded <br> <br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Nama Hewan: $kodok->name <br>"; // "shaun"
    echo "Jumlah Kaki: $kodok->legs <br>"; // 4
    echo "Berdarah Dingin: $kodok->cold_blooded <br>"; // "no"
    echo $kodok->jump(); // "HOP HOP"

    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan: $sungokong->name <br>"; // "shaun"
    echo "Jumlah Kaki: $sungokong->legs <br>"; // 4
    echo "Berdarah Dingin: $sungokong->cold_blooded <br>"; // "no"
    echo $sungokong->yell(); // "Auooo"


?>